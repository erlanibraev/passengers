package com.passengersfriend.test.service;

import com.passengersfriend.test.PassengersFriendProperties;
import com.passengersfriend.test.service.impl.PassengersFriendUtilsDefault;
import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.HashSet;
import java.util.StringJoiner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Erlan Ibraev on 27.10.2018.
 */
public class PassengersFriendUtilsTest {

    private PassengersFriendUtils utils;

    @Before
    public void init() {
        PassengersFriendProperties properties = new PassengersFriendProperties();
        properties.setDateFormat("dd/MM/yy HH:mm:ss");

        utils = new PassengersFriendUtilsDefault(properties);
    }

    @Test
    public void testTimestamp() throws ParseException {
        Timestamp timestamp = Timestamp.valueOf("2018-10-27 12:12:12");
        Timestamp result = utils.toTimestamp("27/10/18", "12:12:12");
        System.out.println(result);
        assertThat(timestamp).isEqualTo(result);
    }

    @Test
    public void strintToListAllNullTest() {
        String result = utils.stringToList(null, null);
        System.out.println(result);
        assertThat(result).isNullOrEmpty();
    }

    @Test
    public void stringToListToIsNotNullFromIsNullTest() {
        String result = utils.stringToList("123, 321", null);
        System.out.println(result);
        assertThat(result).isEqualTo("123, 321");
    }

    @Test
    public void stringToListToIsNullFromIsNotNullTest() {
        String result = utils.stringToList(null, "123");
        System.out.println(result);
        assertThat(result).isEqualTo("123");
    }

    @Test
    public void stringToListToIsListFromIsNotNull() {
        HashSet<String> set = new HashSet<>();
        set.add("1");
        set.add("2");
        set.add("3");
        set.add("4");
        set.add("123");
        StringJoiner joiner = new StringJoiner(",");
        for(String item: set) {
            joiner.add(item);
        }
        String test = joiner.toString();

        String result = utils.stringToList("1,2,3,4", "123");
        System.out.println(result);
        assertThat(result).isEqualTo(test);
    }

    @Test
    public void stringToListToIsListFromIsList() {
        HashSet<String> set = new HashSet<>();
        set.add("1");
        set.add("2");
        set.add("3");
        set.add("4");
        set.add("123");
        set.add("321");
        StringJoiner joiner = new StringJoiner(",");
        for(String item: set) {
            joiner.add(item);
        }
        String test = joiner.toString();

        String result = utils.stringToList("1,2,3,4", "123,321");
        System.out.println(result);
        assertThat(result).isEqualTo(test);
    }

    @Test
    public void stringToListWithStopChar() {
        HashSet<String> set = new HashSet<>();
        set.add("1");
        set.add("2");
        set.add("3");
        set.add("4");
        StringJoiner joiner = new StringJoiner(",");
        for(String item: set) {
            joiner.add(item);
        }
        String test = joiner.toString();

        String result = utils.stringToList("1,2,3,4", PassengersFriendUtilsDefault.STOP_STRING);
        System.out.println(result);
        assertThat(result).isEqualTo(test);
    }

    @Test
    public void stringToListNullAndWithStopChar() {
        String result = utils.stringToList(null, PassengersFriendUtilsDefault.STOP_STRING);
        System.out.println(result);
        assertThat(result).isNull();
    }

    @Test
    public void getGreaterAllNullTest() {
        Timestamp result = utils.getGreater(null, null);
        System.out.println(result);
        assertThat(result).isNull();
    }

    @Test
    public void getGreaterOneIsNullTwoIsNotNull() {
        Timestamp testTwo = Timestamp.valueOf("2018-10-27 12:12:12");
        Timestamp result = utils.getGreater(null, testTwo);
        System.out.println(result);
        assertThat(result).isEqualTo(testTwo);
    }

    @Test
    public void getGreaterOneIsNotNullTwoIsNull() {
        Timestamp testOne = Timestamp.valueOf("2018-10-27 12:12:12");
        Timestamp result = utils.getGreater(testOne, null);
        System.out.println(result);
        assertThat(result).isEqualTo(testOne);
    }

    @Test
    public void getGreaterOneGreaterTwo() {
        Timestamp testOne = Timestamp.valueOf("2018-10-27 12:12:12");
        Timestamp testTwo = Timestamp.valueOf("2018-10-27 01:12:12");
        Timestamp result = utils.getGreater(testOne, testTwo);
        System.out.println(result);
        assertThat(result).isEqualTo(testOne);
    }

    @Test
    public void getGreaterTwoGreaterOne() {
        Timestamp testOne = Timestamp.valueOf("2018-10-27 12:12:12");
        Timestamp testTwo = Timestamp.valueOf("2018-10-27 13:13:13");
        Timestamp result = utils.getGreater(testOne, testTwo);
        System.out.println(result);
        assertThat(result).isEqualTo(testTwo);
    }

    @Test
    public void maxIntAllNull() {
        Integer result = utils.maxInt(null, null);
        System.out.println(result);
        assertThat(result).isNull();

    }

    @Test
    public void maxIntOneIsNotNullTwoIsNull() {
        Integer result = utils.maxInt(12, null);
        System.out.println(result);
        assertThat(result).isEqualTo(12);
    }

    @Test
    public void maxIntOneIsNullTwoIsNotNullAndInt() {
        Integer result = utils.maxInt(null, "12");
        System.out.println(result);
        assertThat(result).isEqualTo(12);
    }

    @Test
    public void maxIntOneIsNullTwoIsNotNullAndNotInt() {
        Integer result = utils.maxInt(null, "-");
        System.out.println(result);
        assertThat(result).isNull();
    }

    @Test
    public void maxIntOneIsNotNullTwoIsNotNullAndNotInt() {
        Integer result = utils.maxInt(12, "-");
        System.out.println(result);
        assertThat(result).isEqualTo(12);
    }

    @Test
    public void maxIntOneGreaterTwo() {
        Integer result = utils.maxInt(12, "11");
        System.out.println(result);
        assertThat(result).isEqualTo(12);
    }

    @Test
    public void maxIntTwoGreaterOne() {
        Integer result = utils.maxInt(12, "13");
        System.out.println(result);
        assertThat(result).isEqualTo(13);
    }

    @Test
    public void nextDay() {
        Timestamp day =  Timestamp.valueOf("2018-10-28 12:12:12");
        Timestamp test = Timestamp.valueOf("2018-10-29 00:00:00");
        Timestamp result = utils.nextDay(day);
        System.out.println(result);
        assertThat(result).isEqualTo(test);
    }

}
