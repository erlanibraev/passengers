package com.passengersfriend.test.repository;

import com.passengersfriend.test.entities.DestinationTableEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Erlan Ibraev on 27.10.2018.
 */
@DataJpaTest
@RunWith(SpringRunner.class)
public class DestinationTableRepositoryTest {

    @Autowired
    private DestinationTableRepository repository;

    @Test
    public void firstTest() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Timestamp startDate = new Timestamp(sdf.parse("2018-10-27").getTime());
        Timestamp endDate = new Timestamp(sdf.parse("2018-10-28").getTime());
        List<DestinationTableEntity> result = repository.findByFlightCodeAndFlightNumberAndSchdDepLtBetween("IBE","5225",startDate, endDate);
    }

    @Test
    public void secondTest() {
        List<DestinationTableEntity> result = repository.findByFlightCodeAndFlightNumberAndSchdDepLtIsNull("IBE","5225");
    }

    @Test
    public void thridTest() {
        List<DestinationTableEntity> result = repository.findByFlightCodeAndFlightNumber("IBE","5225");
    }


}
