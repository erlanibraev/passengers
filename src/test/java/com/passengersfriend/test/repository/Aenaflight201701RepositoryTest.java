package com.passengersfriend.test.repository;

import com.google.gson.Gson;
import com.passengersfriend.test.entities.Aenaflight201701Entity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Erlan Ibraev on 21.10.2018.
 */
@DataJpaTest
@RunWith(SpringRunner.class)
public class Aenaflight201701RepositoryTest {

    @Autowired
    private Aenaflight201701Repository repository;

    private final Gson gson = new Gson();

    @Test
    public void firstTest() {
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        Page<Aenaflight201701Entity> result = repository.findAll(PageRequest.of(0,10, sort));
        assert(result.getTotalElements() > 0);
        result.getContent().forEach(entity -> System.out.println(toJson(entity)));
    }

    private String toJson(Object object) {
        return gson.toJson(object);
    }

}
