package com.passengersfriend.test.repository;

import com.passengersfriend.test.entities.ExtractEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Created by Erlan Ibraev on 29.10.2018.
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ExtractRepositoryTest {

    @Autowired
    private ExtractRepository repository;

    @Test
    public void findAllTest() {
        List<ExtractEntity> result = repository.findAll();
        System.out.println(result.size());

    }

    @Test
    public void findAllPageableTest() {
        Page<ExtractEntity> result = repository.findAll(PageRequest.of(2,10));
        System.out.print(result.getTotalElements());
    }
}
