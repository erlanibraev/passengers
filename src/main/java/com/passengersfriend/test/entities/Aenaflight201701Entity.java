package com.passengersfriend.test.entities;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by Erlan Ibraev on 21.10.2018.
 */
@Entity
@Table(name = "aenaflight_2017_01", schema = "public")
public class Aenaflight201701Entity {
    private Long id;
    private String actArrDateTimeLt;
    private String aircraftNameScheduled;
    private String arrAptNameEs;
    private String arrAptCodeIata;
    private String baggageInfo;
    private String carrierAirlineNameEn;
    private String carrierIcaoCode;
    private String carrierNumber;
    private String counter;
    private String depAptNameEs;
    private String depAptCodeIata;
    private String estArrDateTimeLt;
    private String estDepDateTimeLt;
    private String flightAirlineNameEn;
    private String flightAirlineName;
    private String flightIcaoCode;
    private String flightNumber;
    private String fltLegSeqNo;
    private String gateInfo;
    private String loungeInfo;
    private String schdArrOnlyDateLt;
    private String schdArrOnlyTimeLt;
    private String sourceData;
    private String statusInfo;
    private String terminalInfo;
    private String arrTerminalInfo;
    private Long createdAt;
    private String actDepDateTimeLt;
    private String schdDepOnlyDateLt;
    private String schdDepOnlyTimeLt;

    @Id
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "act_arr_date_time_lt")
    public String getActArrDateTimeLt() {
        return actArrDateTimeLt;
    }

    public void setActArrDateTimeLt(String actArrDateTimeLt) {
        this.actArrDateTimeLt = actArrDateTimeLt;
    }

    @Basic
    @Column(name = "aircraft_name_scheduled")
    public String getAircraftNameScheduled() {
        return aircraftNameScheduled;
    }

    public void setAircraftNameScheduled(String aircraftNameScheduled) {
        this.aircraftNameScheduled = aircraftNameScheduled;
    }

    @Basic
    @Column(name = "arr_apt_name_es")
    public String getArrAptNameEs() {
        return arrAptNameEs;
    }

    public void setArrAptNameEs(String arrAptNameEs) {
        this.arrAptNameEs = arrAptNameEs;
    }

    @Basic
    @Column(name = "arr_apt_code_iata")
    public String getArrAptCodeIata() {
        return arrAptCodeIata;
    }

    public void setArrAptCodeIata(String arrAptCodeIata) {
        this.arrAptCodeIata = arrAptCodeIata;
    }

    @Basic
    @Column(name = "baggage_info")
    public String getBaggageInfo() {
        return baggageInfo;
    }

    public void setBaggageInfo(String baggageInfo) {
        this.baggageInfo = baggageInfo;
    }

    @Basic
    @Column(name = "carrier_airline_name_en")
    public String getCarrierAirlineNameEn() {
        return carrierAirlineNameEn;
    }

    public void setCarrierAirlineNameEn(String carrierAirlineNameEn) {
        this.carrierAirlineNameEn = carrierAirlineNameEn;
    }

    @Basic
    @Column(name = "carrier_icao_code")
    public String getCarrierIcaoCode() {
        return carrierIcaoCode;
    }

    public void setCarrierIcaoCode(String carrierIcaoCode) {
        this.carrierIcaoCode = carrierIcaoCode;
    }

    @Basic
    @Column(name = "carrier_number")
    public String getCarrierNumber() {
        return carrierNumber;
    }

    public void setCarrierNumber(String carrierNumber) {
        this.carrierNumber = carrierNumber;
    }

    @Basic
    @Column(name = "counter")
    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }

    @Basic
    @Column(name = "dep_apt_name_es")
    public String getDepAptNameEs() {
        return depAptNameEs;
    }

    public void setDepAptNameEs(String depAptNameEs) {
        this.depAptNameEs = depAptNameEs;
    }

    @Basic
    @Column(name = "dep_apt_code_iata")
    public String getDepAptCodeIata() {
        return depAptCodeIata;
    }

    public void setDepAptCodeIata(String depAptCodeIata) {
        this.depAptCodeIata = depAptCodeIata;
    }

    @Basic
    @Column(name = "est_arr_date_time_lt")
    public String getEstArrDateTimeLt() {
        return estArrDateTimeLt;
    }

    public void setEstArrDateTimeLt(String estArrDateTimeLt) {
        this.estArrDateTimeLt = estArrDateTimeLt;
    }

    @Basic
    @Column(name = "est_dep_date_time_lt")
    public String getEstDepDateTimeLt() {
        return estDepDateTimeLt;
    }

    public void setEstDepDateTimeLt(String estDepDateTimeLt) {
        this.estDepDateTimeLt = estDepDateTimeLt;
    }

    @Basic
    @Column(name = "flight_airline_name_en")
    public String getFlightAirlineNameEn() {
        return flightAirlineNameEn;
    }

    public void setFlightAirlineNameEn(String flightAirlineNameEn) {
        this.flightAirlineNameEn = flightAirlineNameEn;
    }

    @Basic
    @Column(name = "flight_airline_name")
    public String getFlightAirlineName() {
        return flightAirlineName;
    }

    public void setFlightAirlineName(String flightAirlineName) {
        this.flightAirlineName = flightAirlineName;
    }

    @Basic
    @Column(name = "flight_icao_code")
    public String getFlightIcaoCode() {
        return flightIcaoCode;
    }

    public void setFlightIcaoCode(String flightIcaoCode) {
        this.flightIcaoCode = flightIcaoCode;
    }

    @Basic
    @Column(name = "flight_number")
    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    @Basic
    @Column(name = "flt_leg_seq_no")
    public String getFltLegSeqNo() {
        return fltLegSeqNo;
    }

    public void setFltLegSeqNo(String fltLegSeqNo) {
        this.fltLegSeqNo = fltLegSeqNo;
    }

    @Basic
    @Column(name = "gate_info")
    public String getGateInfo() {
        return gateInfo;
    }

    public void setGateInfo(String gateInfo) {
        this.gateInfo = gateInfo;
    }

    @Basic
    @Column(name = "lounge_info")
    public String getLoungeInfo() {
        return loungeInfo;
    }

    public void setLoungeInfo(String loungeInfo) {
        this.loungeInfo = loungeInfo;
    }

    @Basic
    @Column(name = "schd_arr_only_date_lt")
    public String getSchdArrOnlyDateLt() {
        return schdArrOnlyDateLt;
    }

    public void setSchdArrOnlyDateLt(String schdArrOnlyDateLt) {
        this.schdArrOnlyDateLt = schdArrOnlyDateLt;
    }

    @Basic
    @Column(name = "schd_arr_only_time_lt")
    public String getSchdArrOnlyTimeLt() {
        return schdArrOnlyTimeLt;
    }

    public void setSchdArrOnlyTimeLt(String schdArrOnlyTimeLt) {
        this.schdArrOnlyTimeLt = schdArrOnlyTimeLt;
    }

    @Basic
    @Column(name = "source_data")
    public String getSourceData() {
        return sourceData;
    }

    public void setSourceData(String sourceData) {
        this.sourceData = sourceData;
    }

    @Basic
    @Column(name = "status_info")
    public String getStatusInfo() {
        return statusInfo;
    }

    public void setStatusInfo(String statusInfo) {
        this.statusInfo = statusInfo;
    }

    @Basic
    @Column(name = "terminal_info")
    public String getTerminalInfo() {
        return terminalInfo;
    }

    public void setTerminalInfo(String terminalInfo) {
        this.terminalInfo = terminalInfo;
    }

    @Basic
    @Column(name = "arr_terminal_info")
    public String getArrTerminalInfo() {
        return arrTerminalInfo;
    }

    public void setArrTerminalInfo(String arrTerminalInfo) {
        this.arrTerminalInfo = arrTerminalInfo;
    }

    @Basic
    @Column(name = "created_at")
    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    @Basic
    @Column(name = "act_dep_date_time_lt")
    public String getActDepDateTimeLt() {
        return actDepDateTimeLt;
    }

    public void setActDepDateTimeLt(String actDepDateTimeLt) {
        this.actDepDateTimeLt = actDepDateTimeLt;
    }

    @Basic
    @Column(name = "schd_dep_only_date_lt")
    public String getSchdDepOnlyDateLt() {
        return schdDepOnlyDateLt;
    }

    public void setSchdDepOnlyDateLt(String schdDepOnlyDateLt) {
        this.schdDepOnlyDateLt = schdDepOnlyDateLt;
    }

    @Basic
    @Column(name = "schd_dep_only_time_lt")
    public String getSchdDepOnlyTimeLt() {
        return schdDepOnlyTimeLt;
    }

    public void setSchdDepOnlyTimeLt(String schdDepOnlyTimeLt) {
        this.schdDepOnlyTimeLt = schdDepOnlyTimeLt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Aenaflight201701Entity that = (Aenaflight201701Entity) o;
        return id == that.id &&
            Objects.equals(actArrDateTimeLt, that.actArrDateTimeLt) &&
            Objects.equals(aircraftNameScheduled, that.aircraftNameScheduled) &&
            Objects.equals(arrAptNameEs, that.arrAptNameEs) &&
            Objects.equals(arrAptCodeIata, that.arrAptCodeIata) &&
            Objects.equals(baggageInfo, that.baggageInfo) &&
            Objects.equals(carrierAirlineNameEn, that.carrierAirlineNameEn) &&
            Objects.equals(carrierIcaoCode, that.carrierIcaoCode) &&
            Objects.equals(carrierNumber, that.carrierNumber) &&
            Objects.equals(counter, that.counter) &&
            Objects.equals(depAptNameEs, that.depAptNameEs) &&
            Objects.equals(depAptCodeIata, that.depAptCodeIata) &&
            Objects.equals(estArrDateTimeLt, that.estArrDateTimeLt) &&
            Objects.equals(estDepDateTimeLt, that.estDepDateTimeLt) &&
            Objects.equals(flightAirlineNameEn, that.flightAirlineNameEn) &&
            Objects.equals(flightAirlineName, that.flightAirlineName) &&
            Objects.equals(flightIcaoCode, that.flightIcaoCode) &&
            Objects.equals(flightNumber, that.flightNumber) &&
            Objects.equals(fltLegSeqNo, that.fltLegSeqNo) &&
            Objects.equals(gateInfo, that.gateInfo) &&
            Objects.equals(loungeInfo, that.loungeInfo) &&
            Objects.equals(schdArrOnlyDateLt, that.schdArrOnlyDateLt) &&
            Objects.equals(schdArrOnlyTimeLt, that.schdArrOnlyTimeLt) &&
            Objects.equals(sourceData, that.sourceData) &&
            Objects.equals(statusInfo, that.statusInfo) &&
            Objects.equals(terminalInfo, that.terminalInfo) &&
            Objects.equals(arrTerminalInfo, that.arrTerminalInfo) &&
            Objects.equals(createdAt, that.createdAt) &&
            Objects.equals(actDepDateTimeLt, that.actDepDateTimeLt) &&
            Objects.equals(schdDepOnlyDateLt, that.schdDepOnlyDateLt) &&
            Objects.equals(schdDepOnlyTimeLt, that.schdDepOnlyTimeLt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, actArrDateTimeLt, aircraftNameScheduled, arrAptNameEs, arrAptCodeIata, baggageInfo, carrierAirlineNameEn, carrierIcaoCode, carrierNumber, counter, depAptNameEs, depAptCodeIata, estArrDateTimeLt, estDepDateTimeLt, flightAirlineNameEn, flightAirlineName, flightIcaoCode, flightNumber, fltLegSeqNo, gateInfo, loungeInfo, schdArrOnlyDateLt, schdArrOnlyTimeLt, sourceData, statusInfo, terminalInfo, arrTerminalInfo, createdAt, actDepDateTimeLt, schdDepOnlyDateLt, schdDepOnlyTimeLt);
    }
}
