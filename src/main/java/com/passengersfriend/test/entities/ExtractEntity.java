package com.passengersfriend.test.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

/**
 * Created by Erlan Ibraev on 29.10.2018.
 */
@Entity
public class ExtractEntity {
    @Id
    @Column(name = "id")
    private Long id;
    @Column(name = "flight_icao_code")
    private String flightIcaoCode;
    @Column(name = "flight_number")
    private String flightNumber;
    @Column(name = "schd_dep_only_date_lt")
    private String schdDepOnlyDateLt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFlightIcaoCode() {
        return flightIcaoCode;
    }

    public void setFlightIcaoCode(String flightIcaoCode) {
        this.flightIcaoCode = flightIcaoCode;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getSchdDepOnlyDateLt() {
        return schdDepOnlyDateLt;
    }

    public void setSchdDepOnlyDateLt(String schdDepOnlyDateLt) {
        this.schdDepOnlyDateLt = schdDepOnlyDateLt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExtractEntity)) return false;
        ExtractEntity that = (ExtractEntity) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(flightIcaoCode, that.flightIcaoCode) &&
            Objects.equals(flightNumber, that.flightNumber) &&
            Objects.equals(schdDepOnlyDateLt, that.schdDepOnlyDateLt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, flightIcaoCode, flightNumber, schdDepOnlyDateLt);
    }
}
