package com.passengersfriend.test.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Created by Erlan Ibraev on 21.10.2018.
 */
@Entity
@Table(name = "destination_table", schema = "public")
public class DestinationTableEntity {
    private Long id;
    private String adep;
    private String ades;
    private String flightCode;
    private String flightNumber;
    private String carrierCode;
    private String carrierNumber;
    private String statusInfo;
    private Timestamp schdDepLt;
    private Timestamp schdArrLt;
    private Timestamp estDepLt;
    private Timestamp estArrLt;
    private Timestamp actDepLt;
    private Timestamp actArrLt;
    private int fltLegSeqNo;
    private String aircraftNameScheduled;
    private String baggageInfo;
    private String counter;
    private String gateInfo;
    private String loungeInfo;
    private String terminalInfo;
    private String arrTerminalInfo;
    private String sourceData;
    private Timestamp createdAt;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "adep")
    public String getAdep() {
        return adep;
    }

    public void setAdep(String adep) {
        this.adep = adep;
    }

    @Basic
    @Column(name = "ades")
    public String getAdes() {
        return ades;
    }

    public void setAdes(String ades) {
        this.ades = ades;
    }

    @Basic
    @Column(name = "flight_code")
    public String getFlightCode() {
        return flightCode;
    }

    public void setFlightCode(String flightCode) {
        this.flightCode = flightCode;
    }

    @Basic
    @Column(name = "flight_number")
    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    @Basic
    @Column(name = "carrier_code")
    public String getCarrierCode() {
        return carrierCode;
    }

    public void setCarrierCode(String carrierCode) {
        this.carrierCode = carrierCode;
    }

    @Basic
    @Column(name = "carrier_number")
    public String getCarrierNumber() {
        return carrierNumber;
    }

    public void setCarrierNumber(String carrierNumber) {
        this.carrierNumber = carrierNumber;
    }

    @Basic
    @Column(name = "status_info")
    public String getStatusInfo() {
        return statusInfo;
    }

    public void setStatusInfo(String statusInfo) {
        this.statusInfo = statusInfo;
    }

    @Basic
    @Column(name = "schd_dep_lt")
    public Timestamp getSchdDepLt() {
        return schdDepLt;
    }

    public void setSchdDepLt(Timestamp schdDepLt) {
        this.schdDepLt = schdDepLt;
    }

    @Basic
    @Column(name = "schd_arr_lt")
    public Timestamp getSchdArrLt() {
        return schdArrLt;
    }

    public void setSchdArrLt(Timestamp schdArrLt) {
        this.schdArrLt = schdArrLt;
    }

    @Basic
    @Column(name = "est_dep_lt")
    public Timestamp getEstDepLt() {
        return estDepLt;
    }

    public void setEstDepLt(Timestamp estDepLt) {
        this.estDepLt = estDepLt;
    }

    @Basic
    @Column(name = "est_arr_lt")
    public Timestamp getEstArrLt() {
        return estArrLt;
    }

    public void setEstArrLt(Timestamp estArrLt) {
        this.estArrLt = estArrLt;
    }

    @Basic
    @Column(name = "act_dep_lt")
    public Timestamp getActDepLt() {
        return actDepLt;
    }

    public void setActDepLt(Timestamp actDepLt) {
        this.actDepLt = actDepLt;
    }

    @Basic
    @Column(name = "act_arr_lt")
    public Timestamp getActArrLt() {
        return actArrLt;
    }

    public void setActArrLt(Timestamp actArrLt) {
        this.actArrLt = actArrLt;
    }

    @Basic
    @Column(name = "flt_leg_seq_no")
    public int getFltLegSeqNo() {
        return fltLegSeqNo;
    }

    public void setFltLegSeqNo(int fltLegSeqNo) {
        this.fltLegSeqNo = fltLegSeqNo;
    }

    @Basic
    @Column(name = "aircraft_name_scheduled")
    public String getAircraftNameScheduled() {
        return aircraftNameScheduled;
    }

    public void setAircraftNameScheduled(String aircraftNameScheduled) {
        this.aircraftNameScheduled = aircraftNameScheduled;
    }

    @Basic
    @Column(name = "baggage_info")
    public String getBaggageInfo() {
        return baggageInfo;
    }

    public void setBaggageInfo(String baggageInfo) {
        this.baggageInfo = baggageInfo;
    }

    @Basic
    @Column(name = "counter")
    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }

    @Basic
    @Column(name = "gate_info")
    public String getGateInfo() {
        return gateInfo;
    }

    public void setGateInfo(String gateInfo) {
        this.gateInfo = gateInfo;
    }

    @Basic
    @Column(name = "lounge_info")
    public String getLoungeInfo() {
        return loungeInfo;
    }

    public void setLoungeInfo(String loungeInfo) {
        this.loungeInfo = loungeInfo;
    }

    @Basic
    @Column(name = "terminal_info")
    public String getTerminalInfo() {
        return terminalInfo;
    }

    public void setTerminalInfo(String terminalInfo) {
        this.terminalInfo = terminalInfo;
    }

    @Basic
    @Column(name = "arr_terminal_info")
    public String getArrTerminalInfo() {
        return arrTerminalInfo;
    }

    public void setArrTerminalInfo(String arrTerminalInfo) {
        this.arrTerminalInfo = arrTerminalInfo;
    }

    @Basic
    @Column(name = "source_data")
    public String getSourceData() {
        return sourceData;
    }

    public void setSourceData(String sourceData) {
        this.sourceData = sourceData;
    }

    @Basic
    @Column(name = "created_at")
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DestinationTableEntity that = (DestinationTableEntity) o;
        return id == that.id &&
            fltLegSeqNo == that.fltLegSeqNo &&
            Objects.equals(adep, that.adep) &&
            Objects.equals(ades, that.ades) &&
            Objects.equals(flightCode, that.flightCode) &&
            Objects.equals(flightNumber, that.flightNumber) &&
            Objects.equals(carrierCode, that.carrierCode) &&
            Objects.equals(carrierNumber, that.carrierNumber) &&
            Objects.equals(statusInfo, that.statusInfo) &&
            Objects.equals(schdDepLt, that.schdDepLt) &&
            Objects.equals(schdArrLt, that.schdArrLt) &&
            Objects.equals(estDepLt, that.estDepLt) &&
            Objects.equals(estArrLt, that.estArrLt) &&
            Objects.equals(actDepLt, that.actDepLt) &&
            Objects.equals(actArrLt, that.actArrLt) &&
            Objects.equals(aircraftNameScheduled, that.aircraftNameScheduled) &&
            Objects.equals(baggageInfo, that.baggageInfo) &&
            Objects.equals(counter, that.counter) &&
            Objects.equals(gateInfo, that.gateInfo) &&
            Objects.equals(loungeInfo, that.loungeInfo) &&
            Objects.equals(terminalInfo, that.terminalInfo) &&
            Objects.equals(arrTerminalInfo, that.arrTerminalInfo) &&
            Objects.equals(sourceData, that.sourceData) &&
            Objects.equals(createdAt, that.createdAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, adep, ades, flightCode, flightNumber, carrierCode, carrierNumber, statusInfo, schdDepLt, schdArrLt, estDepLt, estArrLt, actDepLt, actArrLt, fltLegSeqNo, aircraftNameScheduled, baggageInfo, counter, gateInfo, loungeInfo, terminalInfo, arrTerminalInfo, sourceData, createdAt);
    }
}
