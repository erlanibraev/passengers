package com.passengersfriend.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by Erlan Ibraev on 21.10.2018.
 */
@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.passengersfriend.test.repository")
@EntityScan(value={"com.passengersfriend.test.entities"})
@EnableTransactionManagement
@EnableConfigurationProperties
public class Main {
    public static void main(String... args) {
        SpringApplication.run(Main.class, args);
    }
}
