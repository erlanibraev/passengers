package com.passengersfriend.test;

import com.passengersfriend.test.service.Extract;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * Created by Erlan Ibraev on 28.10.2018.
 */
@Component
public class PassengersFriendRunner implements CommandLineRunner {

    private static final String RUN = "run";

    private final Extract extract;

    public PassengersFriendRunner(Extract extract) {
        this.extract = extract;
    }

    @Override
    public void run(String... args) {
        if(args.length > 0 && RUN.equals(args[0])) {
            extract.extract();
        } else {
            System.out.println("Usage java -jar Test-1.0-exec.jar run");
        }
    }
}
