package com.passengersfriend.test.service;

/**
 * Created by Erlan Ibraev on 22.10.2018.
 */
public interface Extract {

    void extract();
}
