package com.passengersfriend.test.service;

import com.passengersfriend.test.entities.DestinationTableEntity;

/**
 * Created by Erlan Ibraev on 31.10.2018.
 */
public interface DestinationTableService {

    DestinationTableEntity get(String flightCode, String flightNumber, String schdDepOnlyDateLt);

    void save(DestinationTableEntity forSave);
}
