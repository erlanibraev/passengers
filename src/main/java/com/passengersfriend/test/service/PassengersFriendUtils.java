package com.passengersfriend.test.service;

import java.sql.Timestamp;
import java.text.ParseException;

/**
 * Created by Erlan Ibraev on 27.10.2018.
 */
public interface PassengersFriendUtils {

    String stringToList(String to, String from);
    Timestamp toTimestamp(String stringDate, String stringTime) throws ParseException;
    Timestamp getGreater(Timestamp one, Timestamp two);
    Integer maxInt(Integer one, String two);
    Timestamp nextDay(Timestamp thisDay);
    Timestamp returnNotNull(Timestamp one, Timestamp two);

}
