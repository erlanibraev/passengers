package com.passengersfriend.test.service.impl;

import com.passengersfriend.test.PassengersFriendProperties;
import com.passengersfriend.test.service.PassengersFriendUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.StringJoiner;

/**
 * Created by Erlan Ibraev on 27.10.2018.
 */
@Service
public class PassengersFriendUtilsDefault implements PassengersFriendUtils {

    public static final String STOP_STRING = "-";

    private final Logger log = LoggerFactory.getLogger(PassengersFriendUtilsDefault.class);

    private final String dateFormat;

    public PassengersFriendUtilsDefault( PassengersFriendProperties properties) {
        this.dateFormat = properties.getDateFormat();
    }

    public String stringToList(String to, String from) {
        String result;
        if (to != null && from != null) {
            HashSet<String> setString = new HashSet<>(Arrays.asList(to.split(",")));
            String[] fromList = from.split(",");
            for(String item: fromList) {
                if(!STOP_STRING.equals(item)) {
                    setString.add(item);
                }
            }
            StringJoiner joiner = new StringJoiner(",");
            for(String item: setString) {
                joiner.add(item);
            }
            result = joiner.toString();
        } else if (to != null && from == null) {
            result = to;
        } else {
            result = STOP_STRING.equals(from) ? null : from;
        }
        return result;
    }

    public Timestamp toTimestamp(String stringDate, String stringTime) throws ParseException {
        Timestamp result = Timestamp.valueOf("1900-01-01 00:00:00");
        if(!StringUtils.isEmpty(stringDate)) {
            stringTime = StringUtils.isEmpty(stringTime) ? "00:00:00" : stringTime;
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            java.util.Date date = sdf.parse(stringDate + " " + stringTime);
            result = new Timestamp(date.getTime());
        }
        return result;
    }

    @Override
    public Timestamp getGreater(Timestamp one, Timestamp two) {
        Timestamp result;
        if(one != null) {
            result = two != null && one.before(two)  ? two : one;
        } else {
            result = two;
        }

        return result;
    }

    @Override
    public Integer maxInt(Integer one, String two) {
        Integer result = null;
        try {
            if (one != null) {
                result = Math.max(one, Integer.valueOf(two));
            } else if (two != null) {
                result = Integer.valueOf(two);
            }
        } catch (Exception e) {
            log.warn(e.getLocalizedMessage(), e);
            result = one;
        }
        return result;
    }

    @Override
    public Timestamp nextDay(Timestamp thisDay) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(thisDay);
        cal.add(Calendar.DAY_OF_WEEK, 1);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        return new Timestamp(cal.getTimeInMillis());
    }

    @Override
    public Timestamp returnNotNull(Timestamp one, Timestamp two) {
        return two != null ? two : one;
    }
}
