package com.passengersfriend.test.service.impl;

import com.passengersfriend.test.PassengersFriendProperties;
import com.passengersfriend.test.entities.Aenaflight201701Entity;
import com.passengersfriend.test.entities.DestinationTableEntity;
import com.passengersfriend.test.entities.ExtractEntity;
import com.passengersfriend.test.repository.Aenaflight201701Repository;
import com.passengersfriend.test.repository.ExtractRepository;
import com.passengersfriend.test.service.DestinationTableService;
import com.passengersfriend.test.service.Extract;
import com.passengersfriend.test.service.Transform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by Erlan Ibraev on 22.10.2018.
 */

@Service
public class ExtractDefault implements Extract {

    private static final Logger log = LoggerFactory.getLogger(ExtractDefault.class);

    private final Aenaflight201701Repository repository;
    private final ExtractRepository extractRepository;
    private final Integer startWith;
    private final Integer chunkSize;
    private final DestinationTableService destinationTableService;
    private final Transform transform;

    public ExtractDefault(Aenaflight201701Repository repository,
                          ExtractRepository extractRepository,
                          DestinationTableService destinationTableService,
                          Transform transform,
                          PassengersFriendProperties properties) {
        this.repository = repository;
        this.startWith = properties.getStartWith();
        this.chunkSize = properties.getChunkSize();
        this.destinationTableService = destinationTableService;
        this.transform = transform;
        this.extractRepository = extractRepository;
    }

    @Override
    public void extract() {
        log.info("Extract start");
        Pageable page = PageRequest.of(startWith,chunkSize);
        Page<ExtractEntity> chunk;
        do {
            chunk = extractRepository.findAll(page);
            if (chunk != null && !chunk.getContent().isEmpty()) {
                chunk.getContent().parallelStream().forEach(entity -> {
                    DestinationTableEntity destinationTableEntity = destinationTableService.get(entity.getFlightIcaoCode(), entity.getFlightNumber(), entity.getSchdDepOnlyDateLt());
                    for(Aenaflight201701Entity entity1: repository.findByFlightIcaoCodeAndFlightNumberAndSchdDepOnlyDateLt(entity.getFlightIcaoCode(), entity.getFlightNumber(), entity.getSchdDepOnlyDateLt())) {
                        destinationTableEntity =  transform.transform(destinationTableEntity, entity1);
                    }
                    destinationTableService.save(destinationTableEntity);
                });
            }
            page = chunk.nextPageable();
            log.info("Page "+chunk.getPageable().getPageNumber() + " from "+ chunk.getTotalPages());
        } while(chunk.hasNext());
        log.info("Extract complete");
    }
}
