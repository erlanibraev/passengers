package com.passengersfriend.test.service.impl;

import com.passengersfriend.test.entities.DestinationTableEntity;
import com.passengersfriend.test.repository.DestinationTableRepository;
import com.passengersfriend.test.service.DestinationTableService;
import com.passengersfriend.test.service.PassengersFriendUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Erlan Ibraev on 31.10.2018.
 */
@Service
public class DestinationTableServiceDefault implements DestinationTableService {
    private final Logger log = LoggerFactory.getLogger(DestinationTableServiceDefault.class);
    private final DestinationTableRepository repository;
    private final PassengersFriendUtils utils;

    public DestinationTableServiceDefault(DestinationTableRepository repository, PassengersFriendUtils utils) {
        this.repository = repository;
        this.utils = utils;
    }

    @Override
    public DestinationTableEntity get(String flightCode, String flightNumber, String schdDepOnlyDateLt) {
        DestinationTableEntity result = null;
        try {
            List<DestinationTableEntity> listEntity;
            if (StringUtils.isEmpty(schdDepOnlyDateLt)) {
                listEntity = repository.findByFlightCodeAndFlightNumberAndSchdDepLtIsNull(flightCode, flightNumber);
            } else {
                Timestamp startDate = utils.toTimestamp(schdDepOnlyDateLt, "00:00:00");
                Timestamp endDate = utils.nextDay(startDate);
                listEntity = repository.findByFlightCodeAndFlightNumberAndSchdDepLtBetween(flightCode, flightNumber, startDate, endDate);
            }
            if (listEntity == null) throw new NullPointerException("listEntity is null");

            if (listEntity.isEmpty()) {
                result = new DestinationTableEntity();
                result.setFlightCode(flightCode);
                result.setFlightNumber(flightNumber);
                result.setSchdDepLt(utils.toTimestamp(schdDepOnlyDateLt,"00:00:00"));
            } else {
                result = listEntity.get(0);
            }
        } catch (Exception ex) {
            log.error(ex.getLocalizedMessage(), ex);
        }

        return result;
    }

    @Override
    public void save(DestinationTableEntity forSave) {
        repository.save(forSave);
    }
}
