package com.passengersfriend.test.service.impl;

import com.passengersfriend.test.entities.Aenaflight201701Entity;
import com.passengersfriend.test.entities.DestinationTableEntity;
import com.passengersfriend.test.service.PassengersFriendUtils;
import com.passengersfriend.test.service.Transform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.ParseException;

/**
 * Created by Erlan Ibraev on 21.10.2018.
 */
@Service
@Scope(value = "prototype")
public class TransformDefault implements Transform {

    private final Logger log = LoggerFactory.getLogger(TransformDefault.class);

    private final PassengersFriendUtils utils;

    public TransformDefault(PassengersFriendUtils utils) {
        this.utils = utils;
    }

    @Override
    public DestinationTableEntity transform(DestinationTableEntity oldEntity, Aenaflight201701Entity entity) {
        DestinationTableEntity result = null;

        try {
            String flightCode = entity.getFlightIcaoCode();
            String flightNumber = entity.getFlightNumber();
            String schdDepOnlyDateLt = entity.getSchdDepOnlyDateLt();
            String schdDepOnlyTimeLt = entity.getSchdDepOnlyTimeLt();
            if (oldEntity == null) {
                result = new DestinationTableEntity();
                result.setFlightCode(flightCode);
                result.setFlightNumber(flightNumber);
                result.setSchdDepLt(utils.toTimestamp(schdDepOnlyDateLt, schdDepOnlyTimeLt));
            } else {
                result = oldEntity;
            }

            result = toDestinationTableEntity(result, entity);
        } catch (Exception ex) {
            log.error(ex.getLocalizedMessage(), ex);
        }
        return result;
    }

    private DestinationTableEntity toDestinationTableEntity(DestinationTableEntity result, Aenaflight201701Entity entity) throws ParseException {

        result.setAdep(entity.getDepAptCodeIata() != null ? entity.getDepAptCodeIata() : result.getAdep());
        result.setAdes(entity.getArrAptCodeIata() != null ? entity.getArrAptCodeIata() : result.getAdes());
        result.setCarrierCode(entity.getCarrierIcaoCode() != null ? entity.getCarrierIcaoCode() : result.getCarrierCode());
        result.setCarrierNumber(entity.getCarrierNumber() != null ? entity.getCarrierNumber() : result.getCarrierNumber());
        result.setStatusInfo(utils.stringToList(result.getStatusInfo(), entity.getStatusInfo()));
        result.setSchdArrLt(utils.returnNotNull(result.getSchdArrLt(),utils.toTimestamp(entity.getSchdArrOnlyDateLt(), entity.getSchdArrOnlyTimeLt())));
        result.setEstDepLt(utils.returnNotNull(result.getEstDepLt(), utils.toTimestamp(entity.getEstDepDateTimeLt(), "")));
        result.setEstArrLt(utils.returnNotNull(result.getEstArrLt(), utils.toTimestamp(entity.getEstArrDateTimeLt(), "")));
        result.setActDepLt(utils.returnNotNull(result.getActDepLt(), utils.toTimestamp(entity.getActDepDateTimeLt(), "")));
        result.setActArrLt(utils.returnNotNull(result.getActArrLt(), utils.toTimestamp(entity.getActArrDateTimeLt(), "")));
        result.setFltLegSeqNo(utils.maxInt(result.getFltLegSeqNo(), entity.getFltLegSeqNo()));
        result.setAircraftNameScheduled(utils.stringToList(result.getAircraftNameScheduled(), entity.getAircraftNameScheduled()));
        result.setBaggageInfo(utils.stringToList(result.getBaggageInfo(), entity.getBaggageInfo()));
        result.setCounter(utils.stringToList(result.getCounter(), entity.getCounter()));
        result.setGateInfo(utils.stringToList(result.getGateInfo(), entity.getGateInfo()));
        result.setLoungeInfo(utils.stringToList(result.getLoungeInfo(), entity.getLoungeInfo()));
        result.setTerminalInfo(utils.stringToList(result.getTerminalInfo(), entity.getTerminalInfo()));
        result.setArrTerminalInfo(utils.stringToList(result.getArrTerminalInfo(), entity.getArrTerminalInfo()));
        result.setSourceData(utils.stringToList(result.getSourceData(), entity.getSourceData()));
        result.setCreatedAt(utils.getGreater(result.getCreatedAt(), entity.getCreatedAt() != null ? new Timestamp(entity.getCreatedAt() * 1000) : null));

        return result;
    }

}
