package com.passengersfriend.test.service;

import com.passengersfriend.test.entities.Aenaflight201701Entity;
import com.passengersfriend.test.entities.DestinationTableEntity;

/**
 * Created by Erlan Ibraev on 21.10.2018.
 */
public interface Transform {

    DestinationTableEntity transform(DestinationTableEntity oldEntity, Aenaflight201701Entity entity);

}
