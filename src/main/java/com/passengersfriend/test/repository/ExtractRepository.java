package com.passengersfriend.test.repository;

import com.passengersfriend.test.entities.ExtractEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Erlan Ibraev on 29.10.2018.
 */
@Repository
public interface ExtractRepository extends JpaRepository<ExtractEntity, Long> {

    @Query(nativeQuery = true,
        value = "select flight_icao_code, flight_number, schd_dep_only_date_lt, row_number() over (ORDER BY flight_icao_code, flight_number, schd_dep_only_date_lt) as id\n" +
                "from aenaflight_2017_01\n" +
                "group by flight_icao_code, flight_number, schd_dep_only_date_lt")
    List<ExtractEntity> findAll();

    @Query(nativeQuery = true,
        value = "select flight_icao_code" +
            ", flight_number" +
            ", schd_dep_only_date_lt" +
            ", row_number() over (ORDER BY flight_icao_code, flight_number, schd_dep_only_date_lt) as id\n" +
        "from aenaflight_2017_01\n" +
        "group by flight_icao_code, flight_number, schd_dep_only_date_lt ",
        countQuery = "with t1 as (\n" +
            "    select flight_icao_code, flight_number, schd_dep_only_date_lt\n" +
            "        from aenaflight_2017_01\n" +
            "        group by flight_icao_code, flight_number, schd_dep_only_date_lt\n" +
            ")\n" +
            "select count(*) from t1"
    )
    Page<ExtractEntity> findAll(Pageable pageable);

}
