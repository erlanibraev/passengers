package com.passengersfriend.test.repository;

import com.passengersfriend.test.entities.Aenaflight201701Entity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Erlan Ibraev on 21.10.2018.
 */
@Repository
public interface Aenaflight201701Repository extends JpaRepository<Aenaflight201701Entity, Long> {
    List<Aenaflight201701Entity> findByFlightIcaoCodeAndFlightNumberAndSchdDepOnlyDateLt(String flightIcaoCode, String flightNumber, String schdDepOnlyDateLt);
}
