package com.passengersfriend.test.repository;

import com.passengersfriend.test.entities.DestinationTableEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Erlan Ibraev on 21.10.2018.
 */
@Repository
public interface DestinationTableRepository extends JpaRepository<DestinationTableEntity, Long> {

    List<DestinationTableEntity> findByFlightCodeAndFlightNumberAndSchdDepLtBetween(String filghtCode, String flightNumber, Timestamp startDate, Timestamp endDate);
    List<DestinationTableEntity> findByFlightCodeAndFlightNumberAndSchdDepLtIsNull(String filghtCode, String flightNumber);
    List<DestinationTableEntity> findByFlightCodeAndFlightNumber(String filghtCode, String flightNumber);
}
